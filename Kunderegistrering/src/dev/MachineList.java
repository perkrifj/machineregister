package dev;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import storage.Save;

/**
 * 
 * @author Per Kristian Fjellby
 * @category
 * TO BE INSTANSIATED IN THE GUI-RUNNER CLASS.
 * HANDLES THE STORED LOG LINES WHICH SHOULD BE WRITTEN TO THE GUI
 *
 */
public class MachineList extends ArrayList<LogLine>{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -711369951229516261L;
	public final static String LINEADD = "line";
	public final static String LINEDELETE = "delete";
	
	private ArrayList<LogLine> array;
	private PropertyChangeSupport pcs;
	
	/**
	 * Constructor
	 * Initialize the array
	 */
	public MachineList(){
		pcs = new PropertyChangeSupport(this);
		array = new ArrayList<LogLine>();
		try {
			array = Save.readFromFile();
		} catch (FileNotFoundException e) {
			//FILE NOT FOUND.
			//CREATE THE FILE source.dat in the defualt folder and start program again!
			e.printStackTrace();
		}
	}
	
	/**
	 * Saves the current list to file
	 */
	public void save(){
		try {
			Save.saveToFile(array);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Adds a LogLine to the overall array
	 */
	public void insert(LogLine line) {
		/*
		 * Add line to array
		 * fire property changed
		 */
		array.add(line);
		save();
		System.out.println("Vi har listeners:" + pcs.hasListeners(LINEADD));
		pcs.firePropertyChange(LINEADD, null, line);
	}

	/**
	 * Deletes a LogLine from the overall array
	 */
	public void delete(LogLine line) {
		array.remove(line);
		pcs.firePropertyChange(LINEDELETE, line, null);
		save();
		}
	
	/**
	 * 
	 * @param stringArray
	 * @return an ArrayList of LogLines identified from the search
	 */
	public ArrayList<LogLine> search(String[] stringArray){
		ArrayList<LogLine> searchResults = new ArrayList<LogLine>();
		
		for (int i = 0; i < stringArray.length; i++) {
			System.out.print(stringArray[i]);
		}
		
		if(stringArray[0].isEmpty() == false){
			for (int i = 0; i < array.size(); i++) {
				System.out.println("array0");
				if(array.get(i).getGivenName().equals(stringArray[0])){
					searchResults.add(array.get(i));
				}
			}
		}
		if(stringArray[1].isEmpty() == false){
			for (int i = 0; i < array.size(); i++) {
				System.out.println("array2");
				if(array.get(i).getSurname().equals(stringArray[1])){
					searchResults.add(array.get(i));
				}
			}
		}
		if(stringArray[2].isEmpty() == false){
			for (int i = 0; i < array.size(); i++) {
				if(array.get(i).getEmail().equals(stringArray[2])){
					searchResults.add(array.get(i));
				}
			}
		}
		if(stringArray[3].isEmpty() == false){
			for (int i = 0; i < array.size(); i++) {
				if(array.get(i).getAddress().equals(stringArray[3])){
					searchResults.add(array.get(i));
				}
			}
		}
		if(stringArray[4].isEmpty() == false){
			for (int i = 0; i < array.size(); i++) {
				if(array.get(i).getPostnumber().equals(stringArray[4])){
					searchResults.add(array.get(i));
				}
			}
		}
		if(stringArray[5].isEmpty() == false){
			for (int i = 0; i < array.size(); i++) {
				if(array.get(i).getPostplace().equals(stringArray[5])){
					searchResults.add(array.get(i));
				}
			}
		}
		if(stringArray[6].isEmpty() == false){
			for (int i = 0; i < array.size(); i++) {
				if(array.get(i).getPhoneNumber().equals(stringArray[6])){
					searchResults.add(array.get(i));
				}
			}
		}
		if(stringArray[7].isEmpty() == false){
			for (int i = 0; i < array.size(); i++) {
				if(array.get(i).getMachineType().equals(stringArray[7])){
					searchResults.add(array.get(i));
				}
			}
		}
		if(stringArray[8].isEmpty() == false){
			for (int i = 0; i < array.size(); i++) {
				if(array.get(i).getMachineModel().equals(stringArray[8])){
					searchResults.add(array.get(i));
				}
			}
		}
		if(stringArray[9].isEmpty() == false){
			for (int i = 0; i < array.size(); i++) {
				if(array.get(i).getMachineSerial().equals(stringArray[9])){
					searchResults.add(array.get(i));
				}
			}
		}
		if(stringArray[10].isEmpty() == false){
			for (int i = 0; i < array.size(); i++) {
				if(array.get(i).getDateOfPurchase().equals(stringArray[10])){
					searchResults.add(array.get(i));
				}
			}
		}
		if(stringArray[11].isEmpty() == false){
			for (int i = 0; i < array.size(); i++) {
				if(array.get(i).getEngineType().equals(stringArray[11])){
					searchResults.add(array.get(i));
				}
			}
		}
		if(stringArray[12].isEmpty() == false){
			for (int i = 0; i < array.size(); i++) {
				if(array.get(i).getEngineModel().equals(stringArray[12])){
					searchResults.add(array.get(i));
				}
			}
		}
		if(stringArray[13].isEmpty() == false){
			for (int i = 0; i < array.size(); i++) {
				if(array.get(i).getEngineSerial().equals(stringArray[13])){
					searchResults.add(array.get(i));
				}
			}
		}
		return searchResults;
	}

	/**
	 * returns a specified LogLine from the array
	 * @return 
	 */
	public ArrayList<LogLine> getArray() {
		return this.array;
	}
	
	
	/**
	 *
	 * Add PropertyChangeListener for this class
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener){
		pcs.addPropertyChangeListener(listener);
	}
	
	/**
	 * Remove PropertyChangeListener for this class 
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener){
		pcs.removePropertyChangeListener(listener);
	}
	

}
