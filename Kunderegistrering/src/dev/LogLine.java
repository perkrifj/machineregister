package dev;
import java.io.Serializable;

public class LogLine implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String givenName;
	private String surname;
	private String email;
	private String address;
	private String postnumber;
	private String postplace;
	private String phoneNumber;
	private String machineType;
	private String machineModel;
	private String machineSerial;
	private String engineType;
	private String engineModel;
	private String engineSerial;
	private String dateOfPurchase;
	
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPostnumber() {
		return postnumber;
	}
	public void setPostnumber(String postnumber) {
		this.postnumber = postnumber;
	}
	public String getPostplace() {
		return postplace;
	}
	public void setPostplace(String postplace) {
		this.postplace = postplace;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getMachineType() {
		return machineType;
	}
	public void setMachineType(String machineType) {
		this.machineType = machineType;
	}
	public String getMachineModel() {
		return machineModel;
	}
	public void setMachineModel(String machineModel) {
		this.machineModel = machineModel;
	}
	public String getMachineSerial() {
		return machineSerial;
	}
	public void setMachineSerial(String machineSerial) {
		this.machineSerial = machineSerial;
	}
	public String getEngineType() {
		return engineType;
	}
	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}
	public String getEngineModel() {
		return engineModel;
	}
	public void setEngineModel(String engineModel) {
		this.engineModel = engineModel;
	}
	public String getEngineSerial() {
		return engineSerial;
	}
	public void setEngineSerial(String engineSerial) {
		this.engineSerial = engineSerial;
	}
	public String getDateOfPurchase() {
		return dateOfPurchase;
	}
	public void setDateOfPurchase(String dateOfPurchase) {
		this.dateOfPurchase = dateOfPurchase;
	}
	
	public String toString(){
		return givenName+ " " + surname + " - " + machineType + " " + machineModel;
	}

}
