package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import dev.LogLine;
import dev.MachineList;

public class GUIPanel extends JFrame implements PropertyChangeListener, ListSelectionListener, ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2893650484675082157L;
	private javax.swing.JTextField address;
	private javax.swing.JLabel addressLabel;
	private javax.swing.JButton clearButton;
	private javax.swing.JPanel dataPanel;
	private javax.swing.JTextField dateOfPurchase;
	private javax.swing.JLabel dateOfPurchaseLabel;
	private javax.swing.JTextField email;
	private javax.swing.JLabel emailLabel;
	private javax.swing.JLabel engineHeaderLabel;
	private javax.swing.JTextField engineModel;
	private javax.swing.JTextField engineSerial;
	private javax.swing.JTextField engineType;
	private javax.swing.JTextField givenName;
	private javax.swing.JLabel givenNameLabel;
	private javax.swing.JList<LogLine> guiList;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JLabel machineHeaderLabel;
	private javax.swing.JButton machineListDeleteButton;
	private javax.swing.JTextField machineModel;
	private javax.swing.JPanel machinePanel;
	private javax.swing.JTextField machineSerial;
	private javax.swing.JTextField machineType;
	private javax.swing.JLabel modelHeaderLabel;
	private javax.swing.JTextField phoneNumber;
	private javax.swing.JLabel phonenumberLabel;
	private javax.swing.JTextField postnumber;
	private javax.swing.JLabel postnumberLabel;
	private javax.swing.JTextField postplace;
	private javax.swing.JLabel postplaceLabel;
	private javax.swing.JList searchList;
	private javax.swing.JButton seekButton;
	private javax.swing.JLabel serialHeaderLabel;
	private javax.swing.JButton storeButton;
	private javax.swing.JTextField surname;
	private javax.swing.JLabel surnameLabel;
	private javax.swing.JLabel typeHeaderLabel;
	private javax.swing.JPanel viewPanel;
    private javax.swing.JScrollPane jScrollPane3;

	private DefaultListModel<LogLine> dlm;
	private DefaultListModel<LogLine> dlm2;
	private MachineList machineList;

	private LogLine model;


	public void beansInitComponents(){

		dataPanel = new javax.swing.JPanel();
		givenNameLabel = new javax.swing.JLabel();
		surnameLabel = new javax.swing.JLabel();
		emailLabel = new javax.swing.JLabel();
		addressLabel = new javax.swing.JLabel();
		postplaceLabel = new javax.swing.JLabel();
		postnumberLabel = new javax.swing.JLabel();
		modelHeaderLabel = new javax.swing.JLabel();
		serialHeaderLabel = new javax.swing.JLabel();
		dateOfPurchaseLabel = new javax.swing.JLabel();
		typeHeaderLabel = new javax.swing.JLabel();
		givenName = new javax.swing.JTextField();
		surname = new javax.swing.JTextField();
		email = new javax.swing.JTextField();
		address = new javax.swing.JTextField();
		postnumber = new javax.swing.JTextField();
		postplace = new javax.swing.JTextField();
		dateOfPurchase = new javax.swing.JTextField();
		machineType = new javax.swing.JTextField();
		machineModel = new javax.swing.JTextField();
		machineSerial = new javax.swing.JTextField();
		storeButton = new javax.swing.JButton();
		seekButton = new javax.swing.JButton();
		phonenumberLabel = new javax.swing.JLabel();
		phoneNumber = new javax.swing.JTextField();
		engineModel = new javax.swing.JTextField();
		machineHeaderLabel = new javax.swing.JLabel();
		engineHeaderLabel = new javax.swing.JLabel();
		engineType = new javax.swing.JTextField();
		engineSerial = new javax.swing.JTextField();
		clearButton = new javax.swing.JButton();
		machinePanel = new javax.swing.JPanel();
		machineListDeleteButton = new javax.swing.JButton();
		jScrollPane2 = new javax.swing.JScrollPane();
		//PK ADD
		machineList = new MachineList();
		machineList.addPropertyChangeListener(this);
		dlm = new DefaultListModel<LogLine>();
		guiList = new javax.swing.JList<LogLine>(dlm);
		guiList.addListSelectionListener(this);
		guiList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		//---
		viewPanel = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();
		dlm2 = new DefaultListModel<LogLine>();
		searchList = new javax.swing.JList<LogLine>(dlm2);
		searchList.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent lse) {
				if(searchList.getSelectedValue() != null){
					guiList.clearSelection();
					setModel((LogLine) searchList.getSelectedValue());
					System.out.println("new model set");
				}
			}
		});
		jScrollPane3 = new javax.swing.JScrollPane();
		
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setTitle("Eik Maskinregistrering");

		dataPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Inndata"));
		dataPanel.setToolTipText("");
		dataPanel.setPreferredSize(new java.awt.Dimension(600, 300));

		givenNameLabel.setText("Fornavn:");

		surnameLabel.setText("Etternavn:");

		emailLabel.setText("E-Mail:");

		addressLabel.setText("Adresse:");

		postplaceLabel.setText("Poststed:");

		postnumberLabel.setText("Postnummer:");

		modelHeaderLabel.setText("Modell:");

		serialHeaderLabel.setText("Serienummer:");

		dateOfPurchaseLabel.setText("Kj�psdato:");

		typeHeaderLabel.setText("Fabrikat:");

		storeButton.setText("Lagre");
		/*
		 * PKADD
		 */
		storeButton.addActionListener(this);
		storeButton.setActionCommand("AddLine");
		/*
		 * END PKADD
		 */

		seekButton.setText("S�k");
		seekButton.addActionListener(this);
		seekButton.setActionCommand("search");

		phonenumberLabel.setText("Telefonnummer:");

		machineHeaderLabel.setText("Maskin");

		engineHeaderLabel.setText("Motor");

		//PK
		clearButton.setText("T�m felt");
		clearButton.addActionListener(this);
		clearButton.setActionCommand("delete");
		//PK


		javax.swing.GroupLayout dataPanelLayout = new javax.swing.GroupLayout(dataPanel);
		dataPanel.setLayout(dataPanelLayout);
		dataPanelLayout.setHorizontalGroup(
				dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(dataPanelLayout.createSequentialGroup()
						.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(dataPanelLayout.createSequentialGroup()
										.addGap(36, 36, 36)
										.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
												.addComponent(givenNameLabel)
												.addComponent(surnameLabel)
												.addComponent(emailLabel)))
												.addGroup(dataPanelLayout.createSequentialGroup()
														.addContainerGap()
														.addComponent(phonenumberLabel))
														.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dataPanelLayout.createSequentialGroup()
																.addContainerGap()
																.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																		.addComponent(postplaceLabel, javax.swing.GroupLayout.Alignment.TRAILING)
																		.addComponent(postnumberLabel, javax.swing.GroupLayout.Alignment.TRAILING)
																		.addComponent(addressLabel, javax.swing.GroupLayout.Alignment.TRAILING))))
																		.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																				.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
																						.addComponent(givenName)
																						.addComponent(surname)
																						.addComponent(email)
																						.addComponent(address)
																						.addComponent(phoneNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
																						.addComponent(postplace))
																						.addComponent(postnumber, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
																						.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																								.addGroup(dataPanelLayout.createSequentialGroup()
																										.addGap(30, 30, 30)
																										.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																												.addGroup(dataPanelLayout.createSequentialGroup()
																														.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
																																.addComponent(modelHeaderLabel)
																																.addComponent(typeHeaderLabel)
																																.addComponent(serialHeaderLabel))
																																.addGap(18, 18, 18)
																																.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
																																		.addComponent(machineType, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
																																		.addComponent(machineModel)
																																		.addComponent(machineSerial))
																																		.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																																		.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
																																				.addComponent(engineType, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
																																				.addComponent(engineModel)
																																				.addComponent(engineSerial))
																																				.addContainerGap())
																																				.addGroup(dataPanelLayout.createSequentialGroup()
																																						.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																																								.addGroup(dataPanelLayout.createSequentialGroup()
																																										.addComponent(storeButton)
																																										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																										.addComponent(seekButton, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
																																										.addGap(18, 18, 18)
																																										.addComponent(clearButton))
																																										.addGroup(dataPanelLayout.createSequentialGroup()
																																												.addGap(22, 22, 22)
																																												.addComponent(dateOfPurchaseLabel)
																																												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																																												.addComponent(dateOfPurchase, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
																																												.addGap(0, 0, Short.MAX_VALUE))))
																																												.addGroup(dataPanelLayout.createSequentialGroup()
																																														.addGap(130, 130, 130)
																																														.addComponent(machineHeaderLabel)
																																														.addGap(86, 86, 86)
																																														.addComponent(engineHeaderLabel)
																																														.addContainerGap(77, Short.MAX_VALUE))))
				);
		dataPanelLayout.setVerticalGroup(
				dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(dataPanelLayout.createSequentialGroup()
						.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(givenNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(givenName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(machineHeaderLabel)
								.addComponent(engineHeaderLabel))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(surnameLabel)
										.addComponent(surname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(machineType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(typeHeaderLabel)
										.addComponent(engineType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(emailLabel)
												.addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(modelHeaderLabel)
												.addComponent(machineModel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(engineModel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
																.addComponent(machineSerial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
																.addComponent(serialHeaderLabel)
																.addComponent(engineSerial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
																.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
																		.addComponent(address, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addComponent(addressLabel)))
																		.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																				.addGroup(dataPanelLayout.createSequentialGroup()
																						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																						.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
																								.addComponent(postnumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
																								.addComponent(postnumberLabel))
																								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																								.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
																										.addComponent(postplace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
																										.addComponent(postplaceLabel))
																										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																										.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
																												.addComponent(phoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
																												.addComponent(phonenumberLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
																												.addGroup(dataPanelLayout.createSequentialGroup()
																														.addGap(20, 20, 20)
																														.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
																																.addComponent(dateOfPurchaseLabel)
																																.addComponent(dateOfPurchase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
																																.addGap(18, 18, 18)
																																.addGroup(dataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
																																		.addComponent(seekButton)
																																		.addComponent(storeButton)
																																		.addComponent(clearButton))
																																		.addGap(0, 9, Short.MAX_VALUE))))
				);

		machinePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Maskiner"));

		// PK ADD
		// SLETT EN OPPF�RING FRA MACHINELIST
		machineListDeleteButton.addActionListener(this);
		machineListDeleteButton.setActionCommand("deleteLogLine");
		machineListDeleteButton.setText("Slett");
		
		

		jScrollPane2.setViewportView(guiList);

		javax.swing.GroupLayout machinePanelLayout = new javax.swing.GroupLayout(machinePanel);
		machinePanel.setLayout(machinePanelLayout);
		machinePanelLayout.setHorizontalGroup(
				machinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, machinePanelLayout.createSequentialGroup()
						.addGap(0, 139, Short.MAX_VALUE)
						.addComponent(machineListDeleteButton)
						.addGap(34, 34, 34))
						.addGroup(machinePanelLayout.createSequentialGroup()
								.addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
								.addContainerGap())
				);
		machinePanelLayout.setVerticalGroup(
				machinePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(machinePanelLayout.createSequentialGroup()
						.addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addGap(18, 18, 18)
						.addComponent(machineListDeleteButton))
				);

		 viewPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("S�k"));

	        searchList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
	        jScrollPane3.setViewportView(searchList);

	        javax.swing.GroupLayout viewPanelLayout = new javax.swing.GroupLayout(viewPanel);
	        viewPanel.setLayout(viewPanelLayout);
	        viewPanelLayout.setHorizontalGroup(
	            viewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(viewPanelLayout.createSequentialGroup()
	                .addComponent(jScrollPane3)
	                .addContainerGap())
	        );
	        viewPanelLayout.setVerticalGroup(
	            viewPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(viewPanelLayout.createSequentialGroup()
	                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 324, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(0, 0, Short.MAX_VALUE))
	        );

	        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
	        getContentPane().setLayout(layout);
	        layout.setHorizontalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addComponent(machinePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(viewPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                .addGap(4, 4, 4))
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(dataPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 544, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addContainerGap())
	        );
	        layout.setVerticalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addGap(3, 3, 3)
	                .addComponent(dataPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
	                    .addComponent(machinePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addComponent(viewPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	                .addContainerGap(17, Short.MAX_VALUE))
	        );

	        pack();
	    }

	public void setModel(LogLine l){
		this.model = l;
		if(l != null){
			givenName.setText(l.getGivenName());
			surname.setText(l.getSurname());
			email.setText(l.getEmail());
			address.setText(l.getAddress());
			postnumber.setText(l.getPostnumber());
			postplace.setText(l.getPostplace());
			phoneNumber.setText(l.getPhoneNumber());
			machineType.setText(l.getMachineType());
			machineModel.setText(l.getMachineModel());
			machineSerial.setText(l.getMachineSerial());
			engineType.setText(l.getEngineType());
			engineModel.setText(l.getEngineModel());
			engineSerial.setText(l.getEngineSerial());
			dateOfPurchase.setText(l.getDateOfPurchase());
			System.out.println(getModel());
		}
	}

	public void resetModelAndClearFields(){
		System.out.println("inside reset");
		setModel(null);
		guiList.clearSelection();
		searchList.clearSelection();
		dlm2.removeAllElements();
		givenName.setText(null);
		surname.setText(null);
		email.setText(null);
		address.setText(null);
		postnumber.setText(null);
		postplace.setText(null);
		phoneNumber.setText(null);
		machineType.setText(null);
		machineModel.setText(null);
		machineSerial.setText(null);
		engineType.setText(null);
		engineModel.setText(null);
		engineSerial.setText(null);
		dateOfPurchase.setText(null);
	}
	public LogLine getModel(){
		return this.model;
	}


	public void propertyChange(PropertyChangeEvent pce) {
		if(pce.getPropertyName().equals(MachineList.LINEADD)){
			System.out.println("henter ny val");
			LogLine value = (LogLine) pce.getNewValue();
			dlm.addElement(value);
			guiList.setSelectedValue(value, true);
			setModel(value);
		}
		if (pce.getPropertyName().equals(MachineList.LINEDELETE)) {
			System.out.println("Sletter objekt");
			//Empty the GUI LIST AND ADD NEW OBJETS
			dlm.removeAllElements();
			addElementsToMachineList(0);
			
		}
	}
	
	
	public void addElementsToMachineList(int index){
		for (int i = index; i < machineList.getArray().size(); i++) {
			dlm.addElement(machineList.getArray().get(i));
		}
	}

	public GUIPanel(){
		beansInitComponents();
		//populate lists
		if(machineList.getArray().size() > 0){
			addElementsToMachineList(0);
		}
		//		addActionListeners();
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if (ae.getActionCommand().equals("AddLine")) {
			if(getModel() == null){ 
				System.out.println("logline");
				LogLine temp = new LogLine();

				if(givenName.getText() != null){
					temp.setGivenName(givenName.getText());
				}
				else {
					temp.setGivenName("Not given");
				}

				if(surname.getText() != null){
					temp.setSurname(surname.getText());
				}
				else {
					temp.setSurname("Not given");
				}

				if(email.getText() != null){
					temp.setEmail(email.getText());
				}
				else{
					temp.setEmail("Not given");
				}

				if(address.getText() != null){
					temp.setAddress(address.getText());
				}
				else{
					temp.setAddress("Not given");
				}

				if(postnumber.getText() != null){
					temp.setPostnumber(postnumber.getText());
				}
				else{
					temp.setPostnumber("Not given");
				}

				if(postplace.getText() != null){
					temp.setPostplace(postplace.getText());
				}
				else{
					temp.setPostplace("Not given");
				}

				if(phoneNumber.getText() != null){
					temp.setPhoneNumber(phoneNumber.getText());
				}
				else{
					temp.setPhoneNumber("Not given");
				}

				if(machineType.getText() != null){
					temp.setMachineType(machineType.getText());
				}
				else{
					temp.setMachineType("Not given");
				}

				if(machineModel.getText() != null){
					temp.setMachineModel(machineModel.getText());
				}
				else{
					temp.setMachineModel("Not given");
				}

				if(machineSerial.getText() != null){
					temp.setMachineSerial(machineSerial.getText());
				}
				else{
					temp.setMachineSerial("Not given");
				}

				if(engineType.getText() != null){
					temp.setEngineType(engineType.getText());
				}
				else{
					temp.setEngineType("Not given");
				}

				if(engineModel.getText() != null){
					temp.setEngineModel(engineModel.getText());
				}
				else{
					temp.setEngineModel("Not given");
				}

				if(engineSerial.getText() != null){
					temp.setEngineSerial(engineSerial.getText());
				}
				else{
					temp.setEngineSerial("Not given");
				}

				if(dateOfPurchase.getText() != null){
					temp.setDateOfPurchase(dateOfPurchase.getText());
				}
				else{
					temp.setDateOfPurchase("Not given");
				}
				machineList.insert(temp);
			}
			else{
				System.out.println("inside else");
				if(givenName.getText() != null){
					getModel().setGivenName(givenName.getText());
				}

				if(surname.getText() != null){
					getModel().setSurname(surname.getText());
				}

				if(email.getText() != null){
					getModel().setEmail(email.getText());
				}

				if(address.getText() != null){
					getModel().setAddress(address.getText());
				}

				if(postnumber.getText() != null){
					getModel().setPostnumber(postnumber.getText());
				}

				if(postplace.getText() != null){
					getModel().setPostplace(postplace.getText());
				}

				if(phoneNumber.getText() != null){
					getModel().setPhoneNumber(phoneNumber.getText());
				}

				if(machineType.getText() != null){
					getModel().setMachineType(machineType.getText());
				}
				if(machineModel.getText() != null){
					getModel().setMachineModel(machineModel.getText());
				}

				if(machineSerial.getText() != null){
					getModel().setMachineSerial(machineSerial.getText());
				}

				if(engineType.getText() != null){
					getModel().setEngineType(engineType.getText());
				}

				if(engineModel.getText() != null){
					getModel().setEngineModel(engineModel.getText());
				}

				if(engineSerial.getText() != null){
					getModel().setEngineSerial(engineSerial.getText());
				}

				if(dateOfPurchase.getText() != null){
					getModel().setDateOfPurchase(dateOfPurchase.getText());
				}
				machineList.save();
				repaint();
			}

		}
		if(ae.getActionCommand().equals("delete")){
			resetModelAndClearFields();
		}
		
		if(ae.getActionCommand().equals("deleteLogLine")){
			machineList.delete(getModel());
			resetModelAndClearFields();
//			machineList.save();
		}
		
		if(ae.getActionCommand().equals("search")){
			ArrayList<LogLine> searchResults = machineList.search(generateStringArrayForSearch());
			for (int i = 0; i < searchResults.size(); i++) {
				dlm2.addElement(searchResults.get(i));
			}
		}
			
	}
	
	public String[] generateStringArrayForSearch(){
		String[] sa = new String[14];
		
		if(givenName.getText() != null){
			sa[0] = givenName.getText();
		}
		else{
			sa[0] = null;
		}
		
		if(surname.getText() != null){
			sa[1] = surname.getText();
		}
		else{
			sa[1] = null;
		}
		if(email.getText() != null){
			sa[2] = email.getText();
		}
		else{
			sa[2] = null;
		}
		if(address.getText() != null){
			sa[3] = address.getText();
		}
		else{
			sa[3] = null;
		}
		if(postnumber.getText() != null){
			sa[4] = postnumber.getText();
		}
		else{
			sa[4] = null;
		}
		if(postplace.getText() != null){
			sa[5] = postplace.getText();
		}
		else{
			sa[5] = null;
		}
		if(phoneNumber.getText() != null){
			sa[6] = phoneNumber.getText();
		}
		else{
			sa[6] = null;
		}
		if(machineType.getText() != null){
			sa[7] = machineType.getText();
		}
		else{
			sa[7] = null;
		}
		if(machineModel.getText() != null){
			sa[8] = machineModel.getText();
		}
		else{
			sa[8] = null;
		}
		if(machineSerial.getText() != null){
			sa[9] = machineSerial.getText();
		}
		else{
			sa[9] = null;
		}
		if(dateOfPurchase.getText() != null){
			sa[10] = dateOfPurchase.getText();
		}
		else{
			sa[10] = null;
		}
		if(engineType.getText() != null){
			sa[11] = engineType.getText();
		}
		else{
			sa[11] = null;
		}
		if(engineModel.getText() != null){
			sa[12] = engineModel.getText();
		}
		else{
			sa[12] = null;
		}
		if(engineSerial.getText() != null){
			sa[13] = engineSerial.getText();
		}
		else{
			sa[13] = null;
		}
		
//		for (int i = 0; i < sa.length; i++) {
//			System.out.println(sa[i]);
//			System.out.print("---");
//		}
		return sa;
	}


	public static void main(String[] args) {
		new GUIPanel().setVisible(true);
	}

	@Override
	public void valueChanged(ListSelectionEvent lse) {
		if(guiList.getSelectedValue() != null){
			searchList.clearSelection();
			setModel((LogLine) guiList.getSelectedValue());
			System.out.println("new model set");
		}
		else{
			System.out.println("selected value was null");
		}
	}		
}