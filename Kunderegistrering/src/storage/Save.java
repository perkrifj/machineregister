package storage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import dev.LogLine;


	public class Save {
		
		final static String FILEDIR = "src/storage/source.dat";

		public static void saveToFile(ArrayList list) throws IOException {
				FileOutputStream fos = new FileOutputStream(FILEDIR);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(list);
				oos.flush();
				oos.close();
		}
		public static ArrayList<LogLine> readFromFile() throws FileNotFoundException {
			File f = new File(FILEDIR);
			if(f.length() > 0){
				try {
					FileInputStream fis = new FileInputStream(FILEDIR);
					ObjectInputStream ois = new ObjectInputStream(fis);
					ArrayList<LogLine> list;
					//OK CAST
					list = (ArrayList<LogLine>) ois.readObject();
					return list;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				throw new FileNotFoundException("Klarte ikke � lese fil");
			}
			else{
				ArrayList<LogLine> list = new ArrayList<LogLine>();
				return list;
			}
		}
				
		
		public static void main(String[] args) {
			
			LogLine l1 = new LogLine();
			l1.setGivenName("Per Kristian");
			l1.setSurname("Fjellby");
			l1.setMachineType("Jonsered");
			l1.setMachineModel("LM2146");
			l1.setDateOfPurchase("01.01.2012");
			
			LogLine l2 = new LogLine();
			l2.setGivenName("Peder");
			l2.setSurname("Pedersen");
			l2.setMachineType("Stihl");
			l2.setMachineModel("FC123");
			l2.setDateOfPurchase("06.06.06");
			
			ArrayList<LogLine> liste = new ArrayList<LogLine>();
			liste.add(l1);
			liste.add(l2);
			
			try {
				Save.saveToFile(liste);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			System.out.println("---SKREVET TIL FIL---");
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				ArrayList<LogLine> lestListe;
				lestListe = Save.readFromFile();
				for (int i = 0; i < lestListe.size(); i++) {
					System.out.println(lestListe.get(i).toString());
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}